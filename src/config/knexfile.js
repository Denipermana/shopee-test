// @flow strict
// Update with your config settings.
import path from 'path'
import { env } from '.'

module.exports = {
  development: {
    client: 'mysql2',
    connection: {
      host: env.DATABASE_HOST,
      database: env.DATABASE_NAME,
      user: env.DATABASE_USERNAME,
      password: env.DATABASE_PASSWORD,
    },
    migrations: {
      directory: `${path.join(__dirname, '../', 'database/migrations')}`,
      tableName: 'knex_migrations',
    },
  },

  test: {
    client: 'sqlite3',
    connection: { filename: 'test.sqlite' },
    migrations: {
      directory: `${path.join(__dirname, '../', 'database/migrations')}`,
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: `${path.join(__dirname, '../', 'database/seeds')}`,
      tableName: 'knex_migrations',
    },
    useNullAsDefault: true,
  },

  production: {
    client: 'mysql2',
    connection: {
      host: env.DATABASE_HOST,
      database: env.DATABASE_NAME,
      user: env.DATABASE_USERNAME,
      password: env.DATABASE_PASSWORD,
    },
    migrations: {
      directory: `${path.join(__dirname, '../', 'database/migrations')}`,
      tableName: 'knex_migrations',
    },
  },
}
