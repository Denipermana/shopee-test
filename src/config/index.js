// @flow strict
import {
  bool, cleanEnv, num, str,
} from 'envalid'

export type Env = $ReadOnly<{
  NODE_ENV: string,
  DATABASE_HOST: string,
  DATABASE_USERNAME: string,
  DATABASE_PORT: string,
  DATABASE_NAME: string,
  DATABASE_PASSWORD: string,
  JSON_LOGGING_ENABLED: boolean,
  SERVER_PORT: number,
}>

export type ServerConfiguration = $ReadOnly<{
  port: number,
}>

export type DBConnectionConfiguration = $ReadOnly<{
  name: boolean,
  host: string,
  port: number,
  user: string,
  password: string,
}>

export const env: Env = cleanEnv(process.env, {
  NODE_ENV: str(),
  DATABASE_HOST: str(),
  DATABASE_USERNAME: str(),
  DATABASE_PORT: str(),
  DATABASE_NAME: str(),
  DATABASE_PASSWORD: str(),
  JSON_LOGGING_ENABLED: bool(),
  SERVER_PORT: num(),
})
export const server: ServerConfiguration = {
  port: env.SERVER_PORT,
}

export const environment: string = env.NODE_ENV || 'development'
export const jsonLoggingEnabled: boolean = env.JSON_LOGGING_ENABLED
