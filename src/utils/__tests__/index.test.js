import { slashNotation, namespace } from '../index'

it('should capitalize and remove . to the filename', () => {
  const nameString = 'app.controller'
  expect(namespace(nameString)).toEqual('AppController')
})

it('should return mapped `content`', () => {
  const object = {
    Api: {
      Path: 'content',
    },
  }
  expect(slashNotation('Api/Path', object)).toEqual('content')
})
