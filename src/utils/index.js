// @flow strict
export function namespace(name: string): string {
  return name.replace(/(\b|\.)\w/g, (l: string): string => l.toUpperCase()).replace('.', '')
}

export function slashNotation(string: string, object: object): object {
  return string.split('/').reduce((o, i) => o[i], object)
}
