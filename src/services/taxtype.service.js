/* eslint-disable class-methods-use-this */
// @flow strict

export default class TaxType {
  type: Array<string> = ['Tobacco', 'Food & Beverage', 'Entertainment']

  async getTypeById(id: number): Promise<string> {
    if (id > this.type.length) {
      throw new Error('there is no such type')
    }
    return Promise.resolve(this.type[id - 1])
  }

  getAll(): Array<string> {
    return this.type
  }

  foodTax(amount: number): number {
    return 0.1 * amount
  }

  tobaccoTax(amount: number): number {
    const countPercentage = 0.02 * amount
    return 10 + countPercentage
  }

  entertainmentTax(amount: number): number {
    let tax
    switch (true) {
      case amount >= 100:
        tax = 0.01 * (amount - 100)
        break
      default:
        tax = 0
    }
    return tax
  }
}
