/* eslint-disable class-methods-use-this */
// @flow strict
import { sumBy } from 'lodash'
import TaxType from './taxtype.service'
import BillModel from '../models/bill.model'

type BillsModelResult = {
  bills: BillModel[],
  meta: {
    price_subtotal: number,
    tax_subtotal: number,
    grand_total: number,
  },
}
type PayloadBill = {
  name: string,
  tax_code: number,
  amount: number,
}
export default class BillService {
  taxType: TaxType

  constructor() {
    this.taxType = new TaxType()
  }

  async saveBills(payload: PayloadBill): Promise<BillModel> {
    try {
      const bill: BillModel = await BillModel.query().insertAndFetch({
        name: payload.name,
        type_id: payload.tax_code,
        price: payload.amount,
      })
      return Promise.resolve(bill)
    } catch (err) {
      throw new Error(err.message)
    }
  }

  async getAllBills(): Promise<BillsModelResult> {
    try {
      const bills = await BillModel.query()
      bills.map((element) => {
        let tax
        switch (element.type_id) {
          case 1:
            tax = this.taxType.foodTax(element.price)
            break
          case 2:
            tax = this.taxType.tobaccoTax(element.price)
            break
          case 3:
            tax = this.taxType.entertainmentTax(element.price)
            break
          default:
            tax = 0
        }
        return (element.tax = tax) // eslint-disable-line
      })
      const subTotal = sumBy(bills, bill => bill.price)

      const taxSubTotal = sumBy(bills, bill => bill.tax)
      const result: BillsModelResult = {
        bills,
        meta: {
          price_subtotal: subTotal,
          tax_subtotal: taxSubTotal,
          grand_total: subTotal + taxSubTotal,
        },
      }
      return result
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
