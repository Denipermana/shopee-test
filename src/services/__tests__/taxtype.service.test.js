import TaxType from '../taxtype.service'

let taxtype

beforeEach(() => {
  taxtype = new TaxType()
})
describe('#TaxType', () => {
  it('should return all the types', () => {
    expect(taxtype.getAll()).toEqual(['Tobacco', 'Food & Beverage', 'Entertainment'])
  })
  it('should get type tobaco if for type 1', () => {
    expect(taxtype.getTypeById(1)).resolves.toEqual('Tobacco')
  })

  it('should throw exception if the type is more than 3', () => {
    expect(taxtype.getTypeById(4)).rejects.toEqual(new Error('there is no such type'))
  })

  describe('#Entertainment', () => {
    it('should return tax free if the amount less than 100', () => {
      expect(taxtype.entertainmentTax(90)).toEqual(0)
    })
    it('should return tax free if the amount more than 100', () => {
      expect(taxtype.entertainmentTax(150)).toEqual(0.5)
    })
  })

  describe('#Tobacco', () => {
    it('should return 50 if the amount is 2000', () => {
      expect(taxtype.tobaccoTax(2000)).toEqual(50)
    })
    it('should return 30 if the amount is 1000', () => {
      expect(taxtype.tobaccoTax(1000)).toEqual(30)
    })
  })
  describe('#Food', () => {
    it('should return 100 if the amount is 1000', () => {
      expect(taxtype.foodTax(1000)).toEqual(100)
    })
    it('should return 150 if the amount is 1500', () => {
      expect(taxtype.foodTax(1500)).toEqual(150)
    })
  })
})
