import { Model } from 'objection'
import Knex from 'knex'
import mockDb from 'mock-knex'
import BillService from '../bill.service'
import { environment } from '../../config'
import config from '../../config/knexfile'

let billService

describe('#BillService', () => {
  const knex = Knex(config[environment])
  let tracker = mockDb.getTracker()

  beforeAll(() => {
    Model.knex(knex)
    mockDb.mock(knex)
  })
  beforeEach(() => {
    tracker = mockDb.getTracker()
    tracker.install()

    tracker.on('query', (query) => {
      const results = [
        {
          id: 1,
          type_id: 1,
          name: 'Big Mac',
          price: 1000,
          tax: 100,
          tax_code: 1,
        },
        {
          id: 2,
          type_id: 2,
          name: 'lorem',
          price: 1000,
          tax: 100,
          tax_code: 2,
        },
        {
          id: 3,
          type_id: 3,
          name: 'lorem 2',
          price: 1000,
          tax: 100,
          tax_code: 3,
        },
      ]
      query.response(results)
    })
    billService = new BillService()
  })

  it('should get all bills', async () => {
    const bills = await billService.getAllBills()
    const expected = {
      bills: [
        {
          id: 1,
          type_id: 1,
          name: 'Big Mac',
          price: 1000,
          tax: 100,
          tax_code: 1,
        },
        {
          id: 2,
          name: 'lorem',
          price: 1000,
          tax: 30,
          type_id: 2,
          tax_code: 2,
        },
        {
          id: 3,
          name: 'lorem 2',
          price: 1000,
          tax: 9,
          type_id: 3,
          tax_code: 3,
        },
      ],
      meta: {
        grand_total: 3139,
        price_subtotal: 3000,
        tax_subtotal: 139,
      },
    }
    expect(bills).toEqual(expected)
  })
})
