// @flow strict
import express from 'express'
import bodyParser from 'body-parser'
import compress from 'compression'
import morgan from 'morgan'
import cors from 'cors'
import path from 'path'
import { Model } from 'objection'
import Knex from 'knex'
import createError from 'http-errors'
import httpStatus, { INTERNAL_SERVER_ERROR } from 'http-status'
import { errors } from 'celebrate'
import config from '../config/knexfile'
import routerV1 from '../routes'
import { environment } from '../config'

const knex = Knex(config[environment])
const app = express()
app.disable('x-powered-by')
app.set('views', path.join(__dirname, '../', 'views'))
app.set('view engine', 'pug')
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(compress())
app.use(cors())
app.use('/docs', express.static('docs'))
app.use('/', routerV1)

Model.knex(knex)

app.use((req: $Request, res: $Response, next: NextFunction) => {
  next(createError(httpStatus.NOT_FOUND))
})
app.use(errors())

// error handler
app.use((err, req: $Request, res: $Response, next: NextFunction) => {
  // set locals, only providing error in development
  const status = err.status
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  const response = {
    code: status,
    message: err.message,
  }
  if (req.app.get('env') === 'development') {
    response.stack = err.stack
  }

  // render the error page
  res.status(status || INTERNAL_SERVER_ERROR)
  res.json(response)
})

export default app
