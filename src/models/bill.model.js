// @flow strict
import { Model } from 'objection'
import TaxType from '../services/taxtype.service'

export default class BillModel extends Model {
  static tableName = 'bills'

  $formatJson(json) {
    const taxType = new TaxType()
    json = super.$formatDatabaseJson(json)
    let tax
    switch (json.type_id) {
      case 1:
        tax = taxType.foodTax(json.price)
        break
      case 2:
        tax = taxType.tobaccoTax(json.price)
        break
      case 3:
        tax = taxType.entertainmentTax(json.price)
        break
      default:
        tax = 0
    }
    json.tax = tax
    json.tax_code = json.type_id
    json.amount = json.price
    delete json.tax_id
    delete json.amount
    return json
  }
}
