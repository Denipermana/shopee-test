// @flow strict
import requireDirectory from 'require-directory'
import { namespace, slashNotation } from '../utils'

export const controllers = requireDirectory(module, './', {
  rename: namespace,
  exclude: /.test\.js$/,
})

/**
 * Transform slash notation to the controller's method, appending/prepending any middleware required
 *
 * @export
 * @param {any} ctrllr
 * @returns
 */
export default function (ctrllr: string) {
  const [controllerName: string, methodName: string] = ctrllr.split('@')
  const controller = slashNotation(controllerName, controllers)
  let method
  if (typeof controller[methodName] !== 'undefined') {
    method = controller[methodName]
  }
  const { middleware: ctrlrmiddleware = [] } = controller
  const { middleware = [], posmiddleware = [] } = method
  return [...ctrlrmiddleware, ...middleware, method, ...posmiddleware]
}
