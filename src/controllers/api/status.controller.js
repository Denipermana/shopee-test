// @flow strict
import { type $Request, type $Response } from 'express'

export const middleware = []

export function health(req: $Request, res: $Response) {
  res.json({ message: 'OK' })
}
health.middleware = []
