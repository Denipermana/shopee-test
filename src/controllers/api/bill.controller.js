// @flow strict
import { type $Request, type $Response } from 'express'
import httpStatus, { OK, CREATED } from 'http-status'
import { celebrate } from 'celebrate'
import createError from 'http-errors'
import BillService from '../../services/bill.service'
import createBill from '../../validations/bill.validation'

const billService = new BillService()

export const middleware = []

export async function index(req: $Request, res: $Response) {
  const bills = await billService.getAllBills()
  res.status(OK)
  return res.json(bills)
}

index.middleware = []

export async function add(req: $Request, res: $Response, next) {
  try {
    const result = await billService.saveBills(req.body)
    res.status(CREATED)
    return res.json(result)
  } catch (err) {
    console.log(err)
    next(createError(err.code, err.messge))
  }
}

add.middleware = [celebrate(createBill)]
