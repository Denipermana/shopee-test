import request from 'supertest'
import Knex from 'knex'
import app from '../../../app/express'
import { environment } from '../../../config'
import config from '../../../config/knexfile'

describe('#Bills', () => {
  const knex = Knex(config[environment])
  beforeAll((done) => {
    knex.migrate.rollback().then(() => {
      knex.migrate.latest().then(() => {
        done()
      })
    })
  })
  afterAll((done) => {
    knex.migrate.rollback().then(() => {
      done()
    })
  })
  it('should show 400 if tax_code is not defined', async () => {
    await request(app)
      .post('/api/bills/')
      .send({ name: 'Big mac', amount: 1000 })
      .expect(400)
      .then((res) => {
        const { message } = res.body
        expect(message).toContain('tax_code')
      })
  })
  it('should show 400 if name is not defined', async () => {
    await request(app)
      .post('/api/bills/')
      .send({ amount: 1000 })
      .expect(400)
      .then((res) => {
        const { message } = res.body
        expect(message).toContain('name')
      })
  })
  it('should show 400 if amount is not defined', async () => {
    await request(app)
      .post('/api/bills/')
      .send({ name: 'big mac', tax_code: 1 })
      .expect(400)
      .then((res) => {
        const { message } = res.body
        expect(message).toContain('amount')
      })
  })
  it('should show 201 request success', async () => {
    await request(app)
      .post('/api/bills/')
      .send({ name: 'big mac', tax_code: 1, amount: 1000 })
      .expect(201)
      .then((res) => {
        expect(res.body.name).toEqual('big mac')
        expect(res.body.tax_code).toEqual(1)
        expect(res.body.price).toEqual(1000)
        expect(res.body.tax).toEqual(100)
      })
  })
  it('should show 200 request success', async (done) => {
    await request(app)
      .get('/api/bills/')
      .expect(200)
      .then((res) => {
        const { bills } = res.body
        expect(bills).toEqual([
          {
            id: 1,
            name: 'big mac',
            price: 1000,
            tax: 100,
            tax_code: 1,
            type_id: 1,
          },
        ])
        done()
      })
  })
})
