// @flow strict
import request from 'supertest'
import app from '../../../app/express'

describe('#Status', () => {
  it('should return 200 /status/health if initialized', (done) => {
    request(app)
      .get('/api/status/health')
      .expect(200, done)
  })
})
