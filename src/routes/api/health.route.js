// @flow strict
import { Router } from 'express'
import Controllers from '../../controllers'

const router: Router = Router()

/**
 * @api {get} /status/health Health Check
 * @apiDescription Register a new user
 * @apiVersion 1.0.0
 * @apiName Health check
 * @apiGroup Status
 * @apiPermission public
 * @apiSampleRequest /status/health
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/status/health
 */

router.get('/health', Controllers('Api/StatusController@health'))

export default router
