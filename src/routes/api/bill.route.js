// @flow strict
import { Router } from 'express'
import Controllers from '../../controllers'

const router: Router = Router()

/**
 * @api {get} bills/ Get my bills
 * @apiVersion 0.1.0
 * @apiName Get all Bills
 * @apiGroup Bills
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
    "bills": [
        {
            "id": 1,
            "type_id": 1,
            "name": "Big Mac",
            "price": 1000,
            "tax": 100,
            "tax_code": 1
        },
        {
            "id": 2,
            "type_id": 2,
            "name": "Marlboro",
            "price": 1000,
            "tax": 30,
            "tax_code": 2
        },
        {
            "id": 3,
            "type_id": 3,
            "name": "21 Cineplex",
            "price": 150,
            "tax": 0.5,
            "tax_code": 3
        },
        {
            "id": 4,
            "type_id": 2,
            "name": "HokBen",
            "price": 2500,
            "tax": 60,
            "tax_code": 2
        }
    ],
    "meta": {
        "price_subtotal": 4650,
        "tax_subtotal": 190.5,
        "grand_total": 4840.5
    }
}
 *
 */

router.get('/', Controllers('Api/BillController@index'))
/**
 * @api {post} bills/ add my bill
 * @apiVersion 0.1.0
 * @apiName Add Bill
 * @apiGroup Bills
 *
 *
 * @apiSuccess {String} name Product name.
 * @apiSuccess {Integer} tax_code Tax.
 * @apiSuccess {Integer} amount Bill amount.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "name": "Burger",
 *       "tax_code": 1,
 *       "amount": 1000
 *     }
 *
 * @apiError ValidationError tax_id is not exists.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *          "statusCode": 400,
 *          "error": "Bad Request",
 *          "message": "child \"tax_code\" fails because [\"tax_code\" is required]",
 *          "validation": {
 *              "source": "body",
 *              "keys": [
 *                  "tax_code"
 *              ]
 *           }
 *     }
 * @apiSampleRequest somurl
 */
router.post('/', Controllers('Api/BillController@add'))

export default router
