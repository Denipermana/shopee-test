// @flow strict
import { Router } from 'express'
import apiHealthRoute from './api/health.route'
import apiBillRoute from './api/bill.route'

const router = Router()

// Routes will be defined here
// Api Routes
router.use('/api/status', apiHealthRoute)
router.use('/api/bills', apiBillRoute)
export default router
