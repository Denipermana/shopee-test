import Joi from 'joi'

const createBill = {
  body: Joi.object()
    .keys({
      name: Joi.string().required(),
      tax_code: Joi.number()
        .min(1)
        .max(3)
        .required(),
      amount: Joi.number()
        .min(1)
        .required(),
    })
    .with('username', 'birthyear')
    .without('password', 'access_token'),
}

export default createBill
