// @flow strict
import * as Promise from 'bluebird' // eslint-disable-line
import app from './app/express'
import { environment, server } from './config'

app.listen(server.port, () => {
  console.log(`Server start on port ${server.port} (${environment})`)
})
