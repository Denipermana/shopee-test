module.exports = {
  coverageReporters: ['html', 'lcov', 'text'],
  coverageThreshold: {
    global: {
      branches: 65,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  collectCoverageFrom: ['src/**/*.{js}'],
  coveragePathIgnorePatterns: ['/src/database/', 'src/models', 'src/app/express.js'],
}
