# REST API

[![pipeline status](https://gitlab.com/Denipermana/shopee-test/badges/master/pipeline.svg)](https://gitlab.com/Denipermana/shopee-test/commits/master)

## To start

Clone the repo

```bash
git clone git@gitlab.com:Denipermana/shopee-test.git
cd shopee-test
```

Run Docker compose:

```bash
docker-compose up -d
```

To test

```bash
yarn test
```

## Docs

- [Api Doc](https://denipermana.gitlab.io/shopee-test/docs/)

## Coverage Report

- [Coverage Report](https://denipermana.gitlab.io/shopee-test/coverage/)

## NOTES

on first build usually its failing because mysql image is just finish building and is not ready for accepting connection. if this happens please try to restart the docker-compose up -d command
