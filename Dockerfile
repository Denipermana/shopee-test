FROM node:10.11-alpine

ENV NODE_ENV development

WORKDIR /opt/app

ADD package.json yarn.lock /opt/app/
RUN yarn install --pure-lockfile

COPY . .

EXPOSE 3000